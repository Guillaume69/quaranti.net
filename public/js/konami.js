var allowedKeys = {
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
  65: 'a',
  66: 'b'
};
var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
var konamiCodePosition = 0;

document.addEventListener('keydown', function (e) {
  var key = allowedKeys[e.keyCode];
  var requiredKey = konamiCode[konamiCodePosition];

  if (key == requiredKey) {
    konamiCodePosition++;
    if (konamiCodePosition == konamiCode.length) {
      wooooaaaahMaisQuestCeQueCestQueCeTrucLa();
      konamiCodePosition = 0;
    }
  } else {
    konamiCodePosition = 0;
  }
});

function wooooaaaahMaisQuestCeQueCestQueCeTrucLa () {
  var video = document.getElementById("sylvain-durif-woooooaaah");
  var container = document.getElementById("sylvain-durif-container");

  container.classList.remove("hidden");
  container.classList.add("full-screen");
  video.play();
  video.onended = function () {
    container.classList.remove("full-screen");
    container.classList.add("hidden");
  }
}
